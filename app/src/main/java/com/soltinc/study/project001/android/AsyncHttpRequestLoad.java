package com.soltinc.study.project001.android;

import android.os.AsyncTask;

import java.util.Map;

/**
 * Created by takagikai on 6/24/16.
 */
public class AsyncHttpRequestLoad extends AsyncTask<String, String, String> {

    private MainActivity mainActivity;

    public AsyncHttpRequestLoad(MainActivity activity) {

        // 呼び出し元のアクティビティ
        this.mainActivity = activity;
    }

    // このメソッドは必ずオーバーライドする必要があるよ
    // ここが非同期で処理される部分みたいたぶん。
    @Override
    protected String doInBackground(String... params) {
        //7, GAEからデータを取得し、取得したデータを返す
        try {
            return null;
            //String sjson = NetUtils.doGet(Consts.TARGET_URL + "/data?cmd=loadData");
            //return sjson;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    // このメソッドは非同期処理の終わった後に呼び出されます
    @Override
    protected void onPostExecute(String result) {
        try {
            //8, doInBackgroundでデータが取得されたら、そのデータをmainActivityに返す
            //mainActivity.loadDataDone(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}