package com.soltinc.study.project001.android;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class NetUtils {

    public static String doGet(String surl) throws IOException {
        URL url = new URL(surl);

        HttpURLConnection connection = null;
        StringBuffer buf = new StringBuffer();

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Map<String, List<String>> map = connection.getHeaderFields();
                InputStreamReader isr = new InputStreamReader(
                        connection.getInputStream(), "UTF8");
                BufferedReader reader = new BufferedReader(isr);
                String line;
                while ((line = reader.readLine()) != null) {
                    buf.append(line + "¥r¥n");
                }
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return buf.toString();
    }



    public static String doPost(String surl, Map<String, Object> params) {
        try {
            URL url = new URL(surl);

            HttpURLConnection connection = null;

            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

                Iterator<String> ite = params.keySet().iterator();
                boolean isIn = false;
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(bout, "UTF8"));
                while (ite.hasNext()) {
                    String key = ite.next();
                    Object val = params.get(key);
                    if (isIn) {
                        writer.write("&");
                    }
                    isIn = true;
                    writer.write(key + "=" + val);
                }
                writer.flush();
                connection.setRequestProperty("Content-Length",
                        "" + bout.toByteArray().length);

                connection.connect();
                writer = new BufferedWriter(new OutputStreamWriter(
                        connection.getOutputStream(), "UTF8"));
                writer.write(new String(bout.toByteArray(), "UTF8"));
                writer.flush();
                writer.close();

                // if (connection.getResponseCode() ==
                // HttpURLConnection.HTTP_OK) {
                InputStreamReader
                        isr = new InputStreamReader(
                        connection.getInputStream(), "UTF8");
                BufferedReader reader = new BufferedReader(isr);
                String line;
                StringBuffer buff = new StringBuffer();
                while ((line = reader.readLine()) != null) {
                    buff.append(line + "\r\n");
                }
                return buff.toString();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }

    }
}
