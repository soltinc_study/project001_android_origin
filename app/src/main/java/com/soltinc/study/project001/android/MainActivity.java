package com.soltinc.study.project001.android;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.graphics.Color;
import android.widget.TextView;
import android.content.res.Resources;
import android.view.Gravity;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import org.json.JSONObject;
import org.json.JSONArray;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //1, 追加ボタンにアクションを設定する
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            //5, 初期化時に、GAEからデータを取得して表示する
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
    private static int cnt = 0;
    private void addData() {

        //2, 投稿するでもデータを作成する
        Map<String, Object> data = new HashMap<String, Object>();
        if (cnt % 5 == 0) {
            data.put("title", "おしゃれキャミ！");
            data.put("category", "レディース > トップス > キャミソール");
            data.put("picture", "" + R.drawable.sample1);
            data.put("description", "今年おすすめの春色カラーを使ったおしゃれなキャミソールです。ここ一番という日にも最適！");
            data.put("price", "51.0");
        } else if (cnt % 5 == 1) {
            data = new HashMap<String, Object>();
            data.put("title", "クールなワンピース");
            data.put("category", "レディース > トップス > ワンピース");
            data.put("picture", "" + R.drawable.sample2);
            data.put("description", "シックなブラックを使った清楚な雰囲気のクールワンピです。ホワイトと合わせれば清純さが際立ちます。");
            data.put("price", "82.0");
        } else if (cnt % 5 == 2) {
            data = new HashMap<String, Object>();
            data.put("title", "ブラックハット");
            data.put("category", "メンズ > アクセサリー > ハット");
            data.put("picture", "" + R.drawable.sample3);
            data.put("description", "今年大流行のハットが登場！ブラックでどんなファッションにも合わせることができます。１点は必ず持っておきたいアイテムです。");
            data.put("price", "46.0");
        } else if (cnt % 5 == 3) {
            data = new HashMap<String, Object>();
            data.put("title", "シースルーのカーディガン");
            data.put("category", "レディース > トップス > カーディガン");
            data.put("picture", "" + R.drawable.sample4);
            data.put("description", "今年おすすめの春色カラーを使ったおしゃれなカーディガンです。ここ一番という日にも最適！");
            data.put("price", "72.5");
        } else if (cnt % 5 == 4) {
            data.put("title", "グレーニット");
            data.put("category", "レディース > トップス > ニット");
            data.put("picture", "" + R.drawable.sample5);
            data.put("description", "今年おすすめのグレーを使ったおしゃれなニットです。ここ一番という日にも最適！");
            data.put("price", "45.0");
        }
        cnt++;

        //3, 保存処理を非同期で呼び出す
        //AsyncHttpRequestSave asyncTask = new AsyncHttpRequestSave(this);
        //asyncTask.execute(data);

    }

    /**
     *
     * @throws Exception
     */
    public void loadData() throws Exception {
        //6, データ取得を非同期で呼び出す
        //AsyncHttpRequestLoad asyncTask = new AsyncHttpRequestLoad(this);
        //asyncTask.execute();
    }
    public void loadDataDone(String sjson) throws Exception {

        //9, 取得したデータをjsonとして解析し、画面に描写する
        LinearLayout layout = (LinearLayout)findViewById(R.id.ListView01);

        layout.removeAllViews();

        Resources res = getResources();
        float textSizeItemTitle = res.getDimension(R.dimen.textSizeItemTitle);
        float textSizeItemCategory = res.getDimension(R.dimen.textSizeItemCategory);
        float textSizeItemDetail = res.getDimension(R.dimen.textSizeItemDetail);
        float textSizePrice = res.getDimension(R.dimen.textSizePrice);
        int textColorPrimary = res.getColor(R.color.colorPrimary);
        int textColorPrimaryLight = res.getColor(R.color.colorPrimaryLight);

        int imageWidth = 400;
        int imageHeight = 400;


        JSONObject json = new JSONObject(sjson);

        JSONArray datas = (JSONArray) json.get("datas");

        for (int i = 0 ; i < datas.length(); i++) {
            JSONObject data = (JSONObject) datas.get(i);

            LinearLayout line = new LinearLayout(this);
            line.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 20);
            line.setLayoutParams(lp);

            ImageView image = new ImageView(this);
            image.setBackgroundColor(Color.rgb(85, 85, 85));
            image.setImageResource((Integer) data.get("picture"));
            image.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageHeight));
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            line.addView(image);

            LinearLayout detail = new LinearLayout(this);
            detail.setOrientation(LinearLayout.VERTICAL);
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(30, 0, 0, 0);
            detail.setLayoutParams(lp);

            TextView text = new TextView(this);
            text.setText((String) data.get("title"));
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 80);
            text.setLayoutParams(lp);
            text.setTextSize(textSizeItemTitle);
            detail.addView(text);

            text = new TextView(this);
            text.setText((String) data.get("category"));
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 60);
            text.setLayoutParams(lp);
            text.setTextColor(textColorPrimaryLight);
            text.setTextSize(textSizeItemCategory);
            detail.addView(text);

            text = new TextView(this);
            text.setText((String) data.get("description"));
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 158);
            text.setLayoutParams(lp);
            text.setTextSize(textSizeItemDetail);
            detail.addView(text);

            text = new TextView(this);
            text.setText("$" + (Double) data.get("price"));
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            text.setLayoutParams(lp);
            text.setGravity(Gravity.RIGHT);
            text.setTextColor(textColorPrimary);
            text.setTextSize(textSizePrice);
            detail.addView(text);

            line.addView(detail);

            layout.addView(line);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
