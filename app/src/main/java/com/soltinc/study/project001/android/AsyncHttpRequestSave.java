package com.soltinc.study.project001.android;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import java.util.Map;

/**
 * Created by takagikai on 6/24/16.
 */
public class AsyncHttpRequestSave extends AsyncTask<Map<String, Object>, String, String> {

    private MainActivity mainActivity;

    public AsyncHttpRequestSave(MainActivity activity) {
        this.mainActivity = activity;
    }

    @Override
    protected String doInBackground(Map<String, Object>... params) {
        //4, 保存処理を呼び出す。このメソッドは非同期で呼び出される
        //※このメソッドが呼び出されると、GAEのテーブルに値が入る。入った値を確認してから、次の処理に進む
        //NetUtils.doPost(Consts.TARGET_URL + "/data?cmd=addData", params[0]);
        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        try {
            //10, データが追加されたのちにも画面の再描写を呼び出すようにする
            //mainActivity.loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}